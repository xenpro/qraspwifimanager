#ifndef QRASPWIFI_H
#define QRASPWIFI_H

#include <QUrl>
#include <QObject>
#include <QQuickItem>
#include <QQmlApplicationEngine>

#include "qwpactrlinterface.h"
#include "qwpascanresultlist.h"
#include "qwpanetworklist.h"

#include <QDebug>

class QRaspWifi: public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString wpaStatusMessage READ getWpaStatus NOTIFY wpaStatusChanged)
    Q_PROPERTY(QJsonObject wpaStatus READ getStatus NOTIFY wpaStatusChanged)
    Q_PROPERTY(RssiState rssi READ getRssi NOTIFY rssiChanged)
public:
    enum RssiState {
        Offline = 0,
        Scaning,
        Acquiring,
        Connected,
        SignalNone,
        SignalWeak,
        SignalOk,
        SignalGood,
        SignalExcellent,
        };
    Q_ENUMS(RssiState)

public:
    QRaspWifi(QQmlApplicationEngine *parent, const QString &rootItemObjectName);

    void setRootQuickObjectName(const QString &objName) { rootQuickObjectName = objName; }
    void setCustomHelpComponent(const QUrl &qmlUrl = QUrl("qrc:/qml/DefaultWifiManager.qml"));

    QJsonObject getStatus() const;

    QString getWpaStatus();
    Q_SIGNAL void wpaStatusChanged();

    Q_INVOKABLE QString getCurrentIface() const;
    Q_INVOKABLE QStringList getInterfaces() const;
    Q_INVOKABLE bool selectInterface(const QString &iface);

    Q_INVOKABLE bool createNewWiFiConnection(const QString &ssid, const QString &psk, const QString bbsid = "any");

    // signal meter
    RssiState getRssi() const;
    void updateRssiState(RssiState state);
    Q_SIGNAL void rssiChanged();

public slots:
    void slot_updateStatus();

    void slot_updateNetworkList();
    void slot_createNetwork(const QString &ssid, const QString &flags, const QString bssid = QString());
    void slot_createNetworkWithPSK(const QString &ssid, const QString &flags, const QString &psk, const QString bssid = "any");
    void slot_selectNetwork(const QString &netId);
    void slot_setDisabledNetwork(const QString &netId, bool disabled);
    void slot_setWpaParam(const QString &netId, const QString &param, const QString &value, bool q = false);
    void slot_wpaGetStatus();
    void slot_startScan();

    void slot_wpaRemoveNetwork(QString id);

    void slot_showWiFiManager();
    void slot_closeWifiManager();

    void slot_saveConfig();

    void slot_DISCONNECT();
    void slot_REASSOCIATE();
    void slot_RECONNECT();

private slots:
    void wpaMessageHandler(QByteArray msg);

    void slot_updateWpaRssiState();

private:
    QQuickItem *getWiFiManagerItem();
    void getScanResult();

private: // WPA
    QWpaCtrlIface *wpaCtrl;

private:
    QTimer *rssiMeterTimer;
    RssiState rssi{Offline};
    QString iface;
    QWpaScanResultList *_scanResultListModel{nullptr};
    QWpaNetworkList *_networkListModel{nullptr};
    QUrl wifiComponentUrl;
    QString rootQuickObjectName;
    QQuickItem *wifitem{nullptr};
    QJsonObject status;
};

#endif // QRASPWIFI_H
