#ifndef QWPANETWORKLIST_H
#define QWPANETWORKLIST_H

#include <QJsonObject>
#include <QAbstractListModel>

class QWpaNetworkList: public QAbstractListModel
{
    Q_OBJECT
public:
    enum WpaNetworkRoles {
        N_BSSID = Qt::UserRole+1,
        N_SSID,
        N_ID,
        N_SIGNAL,
        N_FLAGS,
        N_SELECTED,
        N_DISABLED
    };
    Q_ENUM(WpaNetworkRoles)

public:
    QWpaNetworkList(QObject *parent = nullptr);
    virtual ~QWpaNetworkList();

    virtual QHash<int, QByteArray> roleNames() const;
    virtual int rowCount(const QModelIndex &parent = QModelIndex()) const;
    virtual QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;

    void updateNetworks(const QByteArray &res);

    Q_INVOKABLE void selectNetwork(int index);
    Q_INVOKABLE void removeNetwork(int index);
    Q_INVOKABLE void setDisabledNetwork(int index, bool disabled);
    Q_INVOKABLE void setWpaParam(int index, QString param, QString val, bool quotes = false);

signals:
    Q_SIGNAL void signal_WpaSelectNetwork(QString netId);
    Q_SIGNAL void signal_WpaRemoveNetwork(QString netId);
    Q_SIGNAL void signal_WpaDisableNetwork(QString netId, bool disable);
    Q_SIGNAL void signal_setWpaParam(QString netId, QString param, QString val, bool quotes = false);

private:
    QList<QJsonObject> networkList;
};

#endif // QWPANETWORKLIST_H
