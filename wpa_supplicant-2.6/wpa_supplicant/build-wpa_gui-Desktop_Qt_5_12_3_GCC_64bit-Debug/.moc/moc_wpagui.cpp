/****************************************************************************
** Meta object code from reading C++ file 'wpagui.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../wpa_gui-qt4/wpagui.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'wpagui.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_WpaGuiApp_t {
    QByteArrayData data[1];
    char stringdata0[10];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_WpaGuiApp_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_WpaGuiApp_t qt_meta_stringdata_WpaGuiApp = {
    {
QT_MOC_LITERAL(0, 0, 9) // "WpaGuiApp"

    },
    "WpaGuiApp"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_WpaGuiApp[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

void WpaGuiApp::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject WpaGuiApp::staticMetaObject = { {
    &QApplication::staticMetaObject,
    qt_meta_stringdata_WpaGuiApp.data,
    qt_meta_data_WpaGuiApp,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *WpaGuiApp::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *WpaGuiApp::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_WpaGuiApp.stringdata0))
        return static_cast<void*>(this);
    return QApplication::qt_metacast(_clname);
}

int WpaGuiApp::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QApplication::qt_metacall(_c, _id, _a);
    return _id;
}
struct qt_meta_stringdata_WpaGui_t {
    QByteArrayData data[64];
    char stringdata0[809];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_WpaGui_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_WpaGui_t qt_meta_stringdata_WpaGui = {
    {
QT_MOC_LITERAL(0, 0, 6), // "WpaGui"
QT_MOC_LITERAL(1, 7, 10), // "parse_argv"
QT_MOC_LITERAL(2, 18, 0), // ""
QT_MOC_LITERAL(3, 19, 12), // "updateStatus"
QT_MOC_LITERAL(4, 32, 14), // "updateNetworks"
QT_MOC_LITERAL(5, 47, 9), // "helpIndex"
QT_MOC_LITERAL(6, 57, 12), // "helpContents"
QT_MOC_LITERAL(7, 70, 9), // "helpAbout"
QT_MOC_LITERAL(8, 80, 10), // "disconnect"
QT_MOC_LITERAL(9, 91, 4), // "scan"
QT_MOC_LITERAL(10, 96, 12), // "eventHistory"
QT_MOC_LITERAL(11, 109, 4), // "ping"
QT_MOC_LITERAL(12, 114, 17), // "signalMeterUpdate"
QT_MOC_LITERAL(13, 132, 10), // "processMsg"
QT_MOC_LITERAL(14, 143, 5), // "char*"
QT_MOC_LITERAL(15, 149, 3), // "msg"
QT_MOC_LITERAL(16, 153, 14), // "processCtrlReq"
QT_MOC_LITERAL(17, 168, 11), // "const char*"
QT_MOC_LITERAL(18, 180, 3), // "req"
QT_MOC_LITERAL(19, 184, 11), // "receiveMsgs"
QT_MOC_LITERAL(20, 196, 8), // "connectB"
QT_MOC_LITERAL(21, 205, 13), // "selectNetwork"
QT_MOC_LITERAL(22, 219, 3), // "sel"
QT_MOC_LITERAL(23, 223, 19), // "editSelectedNetwork"
QT_MOC_LITERAL(24, 243, 17), // "editListedNetwork"
QT_MOC_LITERAL(25, 261, 21), // "removeSelectedNetwork"
QT_MOC_LITERAL(26, 283, 19), // "removeListedNetwork"
QT_MOC_LITERAL(27, 303, 10), // "addNetwork"
QT_MOC_LITERAL(28, 314, 17), // "enableAllNetworks"
QT_MOC_LITERAL(29, 332, 18), // "disableAllNetworks"
QT_MOC_LITERAL(30, 351, 17), // "removeAllNetworks"
QT_MOC_LITERAL(31, 369, 10), // "saveConfig"
QT_MOC_LITERAL(32, 380, 13), // "selectAdapter"
QT_MOC_LITERAL(33, 394, 27), // "updateNetworkDisabledStatus"
QT_MOC_LITERAL(34, 422, 19), // "enableListedNetwork"
QT_MOC_LITERAL(35, 442, 20), // "disableListedNetwork"
QT_MOC_LITERAL(36, 463, 15), // "showTrayMessage"
QT_MOC_LITERAL(37, 479, 28), // "QSystemTrayIcon::MessageIcon"
QT_MOC_LITERAL(38, 508, 4), // "type"
QT_MOC_LITERAL(39, 513, 3), // "sec"
QT_MOC_LITERAL(40, 517, 14), // "showTrayStatus"
QT_MOC_LITERAL(41, 532, 14), // "updateTrayIcon"
QT_MOC_LITERAL(42, 547, 12), // "TrayIconType"
QT_MOC_LITERAL(43, 560, 17), // "updateTrayToolTip"
QT_MOC_LITERAL(44, 578, 14), // "loadThemedIcon"
QT_MOC_LITERAL(45, 593, 5), // "names"
QT_MOC_LITERAL(46, 599, 8), // "fallback"
QT_MOC_LITERAL(47, 608, 9), // "wpsDialog"
QT_MOC_LITERAL(48, 618, 11), // "peersDialog"
QT_MOC_LITERAL(49, 630, 10), // "tabChanged"
QT_MOC_LITERAL(50, 641, 5), // "index"
QT_MOC_LITERAL(51, 647, 6), // "wpsPbc"
QT_MOC_LITERAL(52, 654, 14), // "wpsGeneratePin"
QT_MOC_LITERAL(53, 669, 15), // "wpsApPinChanged"
QT_MOC_LITERAL(54, 685, 4), // "text"
QT_MOC_LITERAL(55, 690, 8), // "wpsApPin"
QT_MOC_LITERAL(56, 699, 12), // "addInterface"
QT_MOC_LITERAL(57, 712, 14), // "languageChange"
QT_MOC_LITERAL(58, 727, 13), // "trayActivated"
QT_MOC_LITERAL(59, 741, 33), // "QSystemTrayIcon::ActivationRe..."
QT_MOC_LITERAL(60, 775, 3), // "how"
QT_MOC_LITERAL(61, 779, 10), // "closeEvent"
QT_MOC_LITERAL(62, 790, 12), // "QCloseEvent*"
QT_MOC_LITERAL(63, 803, 5) // "event"

    },
    "WpaGui\0parse_argv\0\0updateStatus\0"
    "updateNetworks\0helpIndex\0helpContents\0"
    "helpAbout\0disconnect\0scan\0eventHistory\0"
    "ping\0signalMeterUpdate\0processMsg\0"
    "char*\0msg\0processCtrlReq\0const char*\0"
    "req\0receiveMsgs\0connectB\0selectNetwork\0"
    "sel\0editSelectedNetwork\0editListedNetwork\0"
    "removeSelectedNetwork\0removeListedNetwork\0"
    "addNetwork\0enableAllNetworks\0"
    "disableAllNetworks\0removeAllNetworks\0"
    "saveConfig\0selectAdapter\0"
    "updateNetworkDisabledStatus\0"
    "enableListedNetwork\0disableListedNetwork\0"
    "showTrayMessage\0QSystemTrayIcon::MessageIcon\0"
    "type\0sec\0showTrayStatus\0updateTrayIcon\0"
    "TrayIconType\0updateTrayToolTip\0"
    "loadThemedIcon\0names\0fallback\0wpsDialog\0"
    "peersDialog\0tabChanged\0index\0wpsPbc\0"
    "wpsGeneratePin\0wpsApPinChanged\0text\0"
    "wpsApPin\0addInterface\0languageChange\0"
    "trayActivated\0QSystemTrayIcon::ActivationReason\0"
    "how\0closeEvent\0QCloseEvent*\0event"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_WpaGui[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      45,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,  239,    2, 0x0a /* Public */,
       3,    0,  240,    2, 0x0a /* Public */,
       4,    0,  241,    2, 0x0a /* Public */,
       5,    0,  242,    2, 0x0a /* Public */,
       6,    0,  243,    2, 0x0a /* Public */,
       7,    0,  244,    2, 0x0a /* Public */,
       8,    0,  245,    2, 0x0a /* Public */,
       9,    0,  246,    2, 0x0a /* Public */,
      10,    0,  247,    2, 0x0a /* Public */,
      11,    0,  248,    2, 0x0a /* Public */,
      12,    0,  249,    2, 0x0a /* Public */,
      13,    1,  250,    2, 0x0a /* Public */,
      16,    1,  253,    2, 0x0a /* Public */,
      19,    0,  256,    2, 0x0a /* Public */,
      20,    0,  257,    2, 0x0a /* Public */,
      21,    1,  258,    2, 0x0a /* Public */,
      23,    0,  261,    2, 0x0a /* Public */,
      24,    0,  262,    2, 0x0a /* Public */,
      25,    0,  263,    2, 0x0a /* Public */,
      26,    0,  264,    2, 0x0a /* Public */,
      27,    0,  265,    2, 0x0a /* Public */,
      28,    0,  266,    2, 0x0a /* Public */,
      29,    0,  267,    2, 0x0a /* Public */,
      30,    0,  268,    2, 0x0a /* Public */,
      31,    0,  269,    2, 0x0a /* Public */,
      32,    1,  270,    2, 0x0a /* Public */,
      33,    0,  273,    2, 0x0a /* Public */,
      34,    1,  274,    2, 0x0a /* Public */,
      35,    1,  277,    2, 0x0a /* Public */,
      36,    3,  280,    2, 0x0a /* Public */,
      40,    0,  287,    2, 0x0a /* Public */,
      41,    1,  288,    2, 0x0a /* Public */,
      43,    1,  291,    2, 0x0a /* Public */,
      44,    2,  294,    2, 0x0a /* Public */,
      47,    0,  299,    2, 0x0a /* Public */,
      48,    0,  300,    2, 0x0a /* Public */,
      49,    1,  301,    2, 0x0a /* Public */,
      51,    0,  304,    2, 0x0a /* Public */,
      52,    0,  305,    2, 0x0a /* Public */,
      53,    1,  306,    2, 0x0a /* Public */,
      55,    0,  309,    2, 0x0a /* Public */,
      56,    0,  310,    2, 0x0a /* Public */,
      57,    0,  311,    2, 0x09 /* Protected */,
      58,    1,  312,    2, 0x09 /* Protected */,
      61,    1,  315,    2, 0x09 /* Protected */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 14,   15,
    QMetaType::Void, 0x80000000 | 17,   18,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,   22,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,   22,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Bool,    2,
    QMetaType::Void, QMetaType::Bool,    2,
    QMetaType::Void, 0x80000000 | 37, QMetaType::Int, QMetaType::QString,   38,   39,   15,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 42,   38,
    QMetaType::Void, QMetaType::QString,   15,
    QMetaType::QIcon, QMetaType::QStringList, QMetaType::QIcon,   45,   46,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,   50,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,   54,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 59,   60,
    QMetaType::Void, 0x80000000 | 62,   63,

       0        // eod
};

void WpaGui::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<WpaGui *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->parse_argv(); break;
        case 1: _t->updateStatus(); break;
        case 2: _t->updateNetworks(); break;
        case 3: _t->helpIndex(); break;
        case 4: _t->helpContents(); break;
        case 5: _t->helpAbout(); break;
        case 6: _t->disconnect(); break;
        case 7: _t->scan(); break;
        case 8: _t->eventHistory(); break;
        case 9: _t->ping(); break;
        case 10: _t->signalMeterUpdate(); break;
        case 11: _t->processMsg((*reinterpret_cast< char*(*)>(_a[1]))); break;
        case 12: _t->processCtrlReq((*reinterpret_cast< const char*(*)>(_a[1]))); break;
        case 13: _t->receiveMsgs(); break;
        case 14: _t->connectB(); break;
        case 15: _t->selectNetwork((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 16: _t->editSelectedNetwork(); break;
        case 17: _t->editListedNetwork(); break;
        case 18: _t->removeSelectedNetwork(); break;
        case 19: _t->removeListedNetwork(); break;
        case 20: _t->addNetwork(); break;
        case 21: _t->enableAllNetworks(); break;
        case 22: _t->disableAllNetworks(); break;
        case 23: _t->removeAllNetworks(); break;
        case 24: _t->saveConfig(); break;
        case 25: _t->selectAdapter((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 26: _t->updateNetworkDisabledStatus(); break;
        case 27: _t->enableListedNetwork((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 28: _t->disableListedNetwork((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 29: _t->showTrayMessage((*reinterpret_cast< QSystemTrayIcon::MessageIcon(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< const QString(*)>(_a[3]))); break;
        case 30: _t->showTrayStatus(); break;
        case 31: _t->updateTrayIcon((*reinterpret_cast< TrayIconType(*)>(_a[1]))); break;
        case 32: _t->updateTrayToolTip((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 33: { QIcon _r = _t->loadThemedIcon((*reinterpret_cast< const QStringList(*)>(_a[1])),(*reinterpret_cast< const QIcon(*)>(_a[2])));
            if (_a[0]) *reinterpret_cast< QIcon*>(_a[0]) = std::move(_r); }  break;
        case 34: _t->wpsDialog(); break;
        case 35: _t->peersDialog(); break;
        case 36: _t->tabChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 37: _t->wpsPbc(); break;
        case 38: _t->wpsGeneratePin(); break;
        case 39: _t->wpsApPinChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 40: _t->wpsApPin(); break;
        case 41: _t->addInterface(); break;
        case 42: _t->languageChange(); break;
        case 43: _t->trayActivated((*reinterpret_cast< QSystemTrayIcon::ActivationReason(*)>(_a[1]))); break;
        case 44: _t->closeEvent((*reinterpret_cast< QCloseEvent*(*)>(_a[1]))); break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject WpaGui::staticMetaObject = { {
    &QMainWindow::staticMetaObject,
    qt_meta_stringdata_WpaGui.data,
    qt_meta_data_WpaGui,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *WpaGui::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *WpaGui::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_WpaGui.stringdata0))
        return static_cast<void*>(this);
    if (!strcmp(_clname, "Ui::WpaGui"))
        return static_cast< Ui::WpaGui*>(this);
    return QMainWindow::qt_metacast(_clname);
}

int WpaGui::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 45)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 45;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 45)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 45;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
