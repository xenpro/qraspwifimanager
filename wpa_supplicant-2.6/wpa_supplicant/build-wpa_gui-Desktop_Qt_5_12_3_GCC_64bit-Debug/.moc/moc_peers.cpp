/****************************************************************************
** Meta object code from reading C++ file 'peers.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../wpa_gui-qt4/peers.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'peers.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_Peers_t {
    QByteArrayData data[30];
    char stringdata0[426];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Peers_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Peers_t qt_meta_stringdata_Peers = {
    {
QT_MOC_LITERAL(0, 0, 5), // "Peers"
QT_MOC_LITERAL(1, 6, 12), // "context_menu"
QT_MOC_LITERAL(2, 19, 0), // ""
QT_MOC_LITERAL(3, 20, 3), // "pos"
QT_MOC_LITERAL(4, 24, 9), // "enter_pin"
QT_MOC_LITERAL(5, 34, 11), // "connect_pbc"
QT_MOC_LITERAL(6, 46, 15), // "learn_ap_config"
QT_MOC_LITERAL(7, 62, 11), // "ctx_refresh"
QT_MOC_LITERAL(8, 74, 13), // "ctx_p2p_start"
QT_MOC_LITERAL(9, 88, 12), // "ctx_p2p_stop"
QT_MOC_LITERAL(10, 101, 14), // "ctx_p2p_listen"
QT_MOC_LITERAL(11, 116, 19), // "ctx_p2p_start_group"
QT_MOC_LITERAL(12, 136, 20), // "ctx_p2p_remove_group"
QT_MOC_LITERAL(13, 157, 15), // "ctx_p2p_connect"
QT_MOC_LITERAL(14, 173, 15), // "ctx_p2p_req_pin"
QT_MOC_LITERAL(15, 189, 16), // "ctx_p2p_show_pin"
QT_MOC_LITERAL(16, 206, 19), // "ctx_p2p_display_pin"
QT_MOC_LITERAL(17, 226, 22), // "ctx_p2p_display_pin_pd"
QT_MOC_LITERAL(18, 249, 17), // "ctx_p2p_enter_pin"
QT_MOC_LITERAL(19, 267, 10), // "properties"
QT_MOC_LITERAL(20, 278, 11), // "ctx_hide_ap"
QT_MOC_LITERAL(21, 290, 11), // "ctx_show_ap"
QT_MOC_LITERAL(22, 302, 23), // "ctx_p2p_show_passphrase"
QT_MOC_LITERAL(23, 326, 24), // "ctx_p2p_start_persistent"
QT_MOC_LITERAL(24, 351, 14), // "ctx_p2p_invite"
QT_MOC_LITERAL(25, 366, 14), // "ctx_p2p_delete"
QT_MOC_LITERAL(26, 381, 14), // "languageChange"
QT_MOC_LITERAL(27, 396, 10), // "closeEvent"
QT_MOC_LITERAL(28, 407, 12), // "QCloseEvent*"
QT_MOC_LITERAL(29, 420, 5) // "event"

    },
    "Peers\0context_menu\0\0pos\0enter_pin\0"
    "connect_pbc\0learn_ap_config\0ctx_refresh\0"
    "ctx_p2p_start\0ctx_p2p_stop\0ctx_p2p_listen\0"
    "ctx_p2p_start_group\0ctx_p2p_remove_group\0"
    "ctx_p2p_connect\0ctx_p2p_req_pin\0"
    "ctx_p2p_show_pin\0ctx_p2p_display_pin\0"
    "ctx_p2p_display_pin_pd\0ctx_p2p_enter_pin\0"
    "properties\0ctx_hide_ap\0ctx_show_ap\0"
    "ctx_p2p_show_passphrase\0"
    "ctx_p2p_start_persistent\0ctx_p2p_invite\0"
    "ctx_p2p_delete\0languageChange\0closeEvent\0"
    "QCloseEvent*\0event"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Peers[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      25,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    1,  139,    2, 0x0a /* Public */,
       4,    0,  142,    2, 0x0a /* Public */,
       5,    0,  143,    2, 0x0a /* Public */,
       6,    0,  144,    2, 0x0a /* Public */,
       7,    0,  145,    2, 0x0a /* Public */,
       8,    0,  146,    2, 0x0a /* Public */,
       9,    0,  147,    2, 0x0a /* Public */,
      10,    0,  148,    2, 0x0a /* Public */,
      11,    0,  149,    2, 0x0a /* Public */,
      12,    0,  150,    2, 0x0a /* Public */,
      13,    0,  151,    2, 0x0a /* Public */,
      14,    0,  152,    2, 0x0a /* Public */,
      15,    0,  153,    2, 0x0a /* Public */,
      16,    0,  154,    2, 0x0a /* Public */,
      17,    0,  155,    2, 0x0a /* Public */,
      18,    0,  156,    2, 0x0a /* Public */,
      19,    0,  157,    2, 0x0a /* Public */,
      20,    0,  158,    2, 0x0a /* Public */,
      21,    0,  159,    2, 0x0a /* Public */,
      22,    0,  160,    2, 0x0a /* Public */,
      23,    0,  161,    2, 0x0a /* Public */,
      24,    0,  162,    2, 0x0a /* Public */,
      25,    0,  163,    2, 0x0a /* Public */,
      26,    0,  164,    2, 0x09 /* Protected */,
      27,    1,  165,    2, 0x09 /* Protected */,

 // slots: parameters
    QMetaType::Void, QMetaType::QPoint,    3,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 28,   29,

       0        // eod
};

void Peers::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<Peers *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->context_menu((*reinterpret_cast< const QPoint(*)>(_a[1]))); break;
        case 1: _t->enter_pin(); break;
        case 2: _t->connect_pbc(); break;
        case 3: _t->learn_ap_config(); break;
        case 4: _t->ctx_refresh(); break;
        case 5: _t->ctx_p2p_start(); break;
        case 6: _t->ctx_p2p_stop(); break;
        case 7: _t->ctx_p2p_listen(); break;
        case 8: _t->ctx_p2p_start_group(); break;
        case 9: _t->ctx_p2p_remove_group(); break;
        case 10: _t->ctx_p2p_connect(); break;
        case 11: _t->ctx_p2p_req_pin(); break;
        case 12: _t->ctx_p2p_show_pin(); break;
        case 13: _t->ctx_p2p_display_pin(); break;
        case 14: _t->ctx_p2p_display_pin_pd(); break;
        case 15: _t->ctx_p2p_enter_pin(); break;
        case 16: _t->properties(); break;
        case 17: _t->ctx_hide_ap(); break;
        case 18: _t->ctx_show_ap(); break;
        case 19: _t->ctx_p2p_show_passphrase(); break;
        case 20: _t->ctx_p2p_start_persistent(); break;
        case 21: _t->ctx_p2p_invite(); break;
        case 22: _t->ctx_p2p_delete(); break;
        case 23: _t->languageChange(); break;
        case 24: _t->closeEvent((*reinterpret_cast< QCloseEvent*(*)>(_a[1]))); break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject Peers::staticMetaObject = { {
    &QDialog::staticMetaObject,
    qt_meta_stringdata_Peers.data,
    qt_meta_data_Peers,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *Peers::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Peers::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_Peers.stringdata0))
        return static_cast<void*>(this);
    if (!strcmp(_clname, "Ui::Peers"))
        return static_cast< Ui::Peers*>(this);
    return QDialog::qt_metacast(_clname);
}

int Peers::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 25)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 25;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 25)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 25;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
