import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12
import QtGraphicalEffects 1.10

import "WiFiComponents"

Page {
    id: root
    anchors.centerIn: parent
    width: parent.width
    height: parent.height
    z: 1

    background:
        Rectangle {
        color: "black"
        opacity: 0.5
    }

    function rssiToStr(rssi) {
        switch (rssi) {
        case 0: return "Нет подключения"
        case 1: return "Поиск сети"
        case 2: return "Установка подключения"
        default: return "Соединение установлено"
        }
    }

    Rectangle {
        height: root.height*.75
        width:  root.width*.75
        anchors.centerIn: parent
        color: "#4400b3"
        border.color: "#3498db"
        border.width: 3
        radius: 15
        ColumnLayout {
            anchors.fill: parent
            anchors.margins: 25
            spacing: 20
            Label {
                id: mainLabel
                text: "Настройки WiFi"
                font.bold: true
                font.family: "Roboto"
                font.pixelSize: 32
                color: "white"
            }
            Label {
                id: wifiState
                text: `Состояние: ${rssiToStr(WiFiCtrl.rssi)}`;
                font.bold: true
                font.family: "Roboto"
                font.pixelSize: 24
                color: "white"
            }
            TabBar {
                id: tabBar
                Layout.fillWidth: true
                background: Rectangle { color: "transparent"; }
                onCurrentIndexChanged: {
                    switch (currentIndex)
                    {
                    case 0:
                        WiFiCtrl.slot_startScan();
                        break;
                    case 1:
                        WiFiCtrl.slot_updateNetworkList();
                        break;
                    }
                }
                TabButton {
                    text: "Поиск сетей";
                    font.pixelSize: 23
                    font.bold: true
                    background: Rectangle {
                        color: tabBar.currentIndex === 0 ? "orange" : "darkgreen"
                        radius: 10
                    }
                }
                TabButton {
                    text: "Подключение"
                    font.pixelSize: 23
                    font.bold: true
                    background: Rectangle {
                        color: tabBar.currentIndex === 1 ? "orange" : "darkgreen"
                        radius: 10
                    }
                }
            }
            StackLayout {
                id: tabsStack
                Layout.fillHeight: true
                Layout.fillWidth : true
                currentIndex: tabBar.currentIndex
                WiFiScanResult {
                    model: WiFiScanResultModel
                    onCreateNewConnection: newConDialog.openCreateNewConnectionDialog(ssid, flags, bssid)
                }
                WiFiNetworkList {
                    id: wifiConnectionTab
                }
            } // StackLayout

            WiFiNewConnDialog {
                id: newConDialog
                width: parent.width*.6
                height: parent.height*.6
                onCreateNewConnectionAccepted: {
                    WiFiCtrl.slot_createNetwork(ssid, flags/*, bssid*/);
                    WiFiCtrl.slot_saveConfig();
                    tabBar.currentIndex = 1; // переключение на вкладку в сетями
                }
            }

            WiFiButton {
                Layout.alignment: Qt.AlignRight
                font.bold: true
                font.family: "Roboto"
                font.pixelSize: 24
                text: "Выход"
                onClick: {
                    WiFiCtrl.slot_closeWifiManager();
                }
            }
        } // ColumnLayout
    }
}
