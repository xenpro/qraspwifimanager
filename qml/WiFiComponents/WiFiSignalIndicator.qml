import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12

ProgressBar {
    id: signalLevelIndicator
    from: -95
    to: -35
    height: parent.height
    width: 30

    background: Rectangle {
        color: "transparent"
    }

    contentItem:
        ColumnLayout {
        id: signalMeterColumn
        height: parent.height
        spacing: 2
        Repeater {
            id: ttt
            model: 5
            Rectangle {
                Layout.alignment: Qt.AlignBottom
                color: "lime"//"#17a81a"
                width: signalMeterColumn.width
                height: 5
                radius: 2
                opacity: index / ttt.model > (1-signalLevelIndicator.visualPosition+.1)? 1: .5
            }
        }
    }
}
