import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12

Dialog {
    id: dialog
    signal createNewConnectionAccepted(var _ssid, var _flags, var _bssid);

    function openCreateNewConnectionDialog(_ssid, _flags, _bssid) {
        ssid = _ssid;
        bssid = _bssid;
        flags = _flags;
        dialog.open();
    }

    visible: false
    anchors.centerIn: parent
    property string ssid
    property string bssid
    property string flags: flags

    background: Rectangle {
        color: "#3498db"
        radius: 15
    }

    contentItem: Rectangle {
        color: "#4400b3"
        width: dialog.width
        ColumnLayout {
            anchors.fill: parent
            anchors.leftMargin: 15
            anchors.rightMargin: 15
            anchors.topMargin: 5
            anchors.bottomMargin: 15
            spacing: 25
            Label {
                id: captionLabel
                font.bold: true
                font.family: "Roboto"
                font.pixelSize: 32
                color: "white"
                text: "Новое соединение"
            }
            Label {
                font.pixelSize: 22
                color: "white"
                text: `Создать подключение к "<b>${dialog.ssid}</b>"?`
            }
            RowLayout {
                Layout.alignment: Qt.AlignCenter
                spacing: 50
                WiFiButton {
                    text: "Создать"
                    font.bold: true
                    font.family: "Roboto"
                    font.pixelSize: 22
                    onClick: dialog.accept()
                }
                WiFiButton {
                    text: "Отмена"
                    font.bold: true
                    font.family: "Roboto"
                    font.pixelSize: 22
                    onClick: dialog.reject()
                }
            }
        }
    }



    onAccepted: {
        createNewConnectionAccepted(ssid, flags, bssid);
    }

    // standardButtons: Dialog.Yes | Dialog.Cancel
} // end dialog
