import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12

ListView {
    id: scanResultListTab
    signal createNewConnection(var ssid, var flags, var bssid);
    clip: true
    spacing: 25
    highlightRangeMode: ListView.NoHighlightRange
    snapMode: ListView.SnapToItem
    keyNavigationWraps: false
    boundsBehavior: Flickable.DragAndOvershootBounds
    highlight: Rectangle
    {
        color:"black"
        radius: 5
        opacity: .5
    }

    Rectangle {
        anchors.fill: parent
        color: "black"
        opacity: 0.1
        radius: 5
    }

    delegate:
        Item {
        width: parent.width
        height: 55
        MouseArea {
            anchors.fill: parent
            onClicked: scanResultListTab.currentIndex = index
            onPressAndHold: createNewConnection(ssid, flags, bssid);
        }
        RowLayout {
            spacing: 20
            height: parent.height
            width: parent.width
            Text {
                id: networkIndexText
                Layout.alignment: Qt.AlignLeft
                Layout.leftMargin: 15
                width: parent.width*.1
                font.family: "Roboto"
                font.pixelSize: 26
                font.bold: true
                color: "white"
                text: index+1
            }
            Text {
                id: ssidField
                Layout.alignment: Qt.AlignVCenter
                Layout.fillWidth: true
                font: networkIndexText.font
                color: "white"
                text: ssid
            }
            Item { Layout.fillWidth: true; }
            Text {
                id: bssidField
                Layout.alignment: Qt.AlignLeft
                font: networkIndexText.font
                color: "white"
                text: `<i>${bssid}</i>`
                MouseArea {
                    anchors.fill: parent
                    onClicked: tooltip.show(flags, 3000);
                }
                ToolTip {
                    id: tooltip
                    anchors.centerIn: parent
                }
            }

            WiFiSignalIndicator {
                id: signalLevelIndicator
                Layout.rightMargin: 15
                value: signal_level
                MouseArea {
                    anchors.fill: parent;
                    onClicked: tooltipSignal.show(`${signalLevelIndicator.value}dB`, 3000);
                }
                ToolTip { id: tooltipSignal; anchors.centerIn: signalLevelIndicator; }
            }
        }
    }
}
