import QtQuick 2.12
import QtGraphicalEffects 1.10

Item  {
    id: ortoButton
    signal click()
    property int size: 150
    property alias pressed: mouseArea.pressed
    property alias text   : buttonText.text
    property alias font   : buttonText.font
    property alias textColor: buttonText.color
    property alias pixelSize: buttonText.font.pixelSize
    implicitHeight: size*.55
    implicitWidth: size
    antialiasing: true

    MouseArea {
        id: mouseArea
        anchors.fill: parent
        onClicked: click()
    }

    Rectangle {
        id: bg
        anchors.centerIn: parent
        height: parent.height*.9
        width: parent.width*.9
        radius: width*.8
        color: ortoButton.enabled?"#3F51B5":"transparent"
        Text {
            id: buttonText
            anchors.centerIn: parent
            layer.enabled: !ortoButton.pressed
            font.pixelSize: parent.height*.25
            color: "white"
            layer.effect: DropShadow {
                horizontalOffset: 5
                verticalOffset: 5
                color: "#80000000"
                radius: 1
                samples: 3
            }
        }
    }

    DropShadow {
        id: shadow
        visible: !mouseArea.pressed && ortoButton.enabled
        anchors.fill: parent
        horizontalOffset: 7
        verticalOffset: 10
        radius: 8.0
        samples: 13
        color: "#80000000"
        source: bg
        z:0
    }
}
