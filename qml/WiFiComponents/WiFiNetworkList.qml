import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12

ListView {
    id: networkList
    Layout.fillWidth: true
    Layout.leftMargin: 25
    Layout.rightMargin: 25
    Layout.fillHeight: true
    model: WiFiNetworkModel

    highlight: Rectangle {
        color:"black"
        radius: 5
        opacity: 0.5
    }

    Rectangle {
        anchors.fill: parent
        color: "black"
        opacity: 0.1
        radius: 5
    }

    delegate: Item {
        id: rootItem
        property bool isPskEdit: false
        width: parent.width
        height: 60
        MouseArea {
            anchors.fill: parent
            onClicked: networkList.currentIndex = index
            onPressAndHold: { networkList.currentIndex = index; contextMenu.y = rootItem.y; contextMenu.popup(); }
        }

        RowLayout {
            visible: rootItem.isPskEdit
            anchors.right: parent.right
            anchors.rightMargin: 15
            width: parent.width*.75
            height: 60
            TextField {
                id: passwordInput
                Layout.fillWidth: true
                font.family: "Roboto"
                font.bold: true
                font.pixelSize: 22
                placeholderText: "Пароль"
                background: Rectangle {
                    color: "white"
                    radius: 5
                }
            }
            Button {
                text: "Сохранить";
                font: passwordInput.font
                onClicked: {
                    rootItem.isPskEdit=false;
                    WiFiNetworkModel.setWpaParam(index, "psk", passwordInput.text, true);
                    WiFiCtrl.slot_REASSOCIATE();
                    WiFiCtrl.slot_saveConfig();
                }
            }
        }

        Menu {
            id: contextMenu
            anchors.centerIn: parent
            title: ssid
            font: counter.font
            MenuItem {
                enabled: !selected
                text: "Выбрать"
                onTriggered: WiFiNetworkModel.selectNetwork(index);
            }

            MenuItem {
                text: "Задать пароль"
                onTriggered: rootItem.isPskEdit = true
            }

            MenuItem {
                text: disabled?"Включить":"Выключить"
                onTriggered: WiFiNetworkModel.setDisabledNetwork(index, !disabled)
            }

            MenuItem {
                text: qsTr("Забыть")
                onTriggered: WiFiNetworkModel.removeNetwork(index);
            }
        }

        RowLayout {
            id: networkFieldLine
            spacing: 15
            anchors.fill: parent

            Text {
                id: counter
                Layout.alignment: Qt.AlignLeft
                Layout.leftMargin: 10
                width: parent.width*.1
                font.family: "Roboto"
                font.pixelSize: 26
                font.bold: true
                font.italic: selected
                color: selected?"yellow":"white"
                text: index+1
            }

            Text {
                id: ssidField
                Layout.alignment: Qt.AlignCenter
                Layout.fillWidth: true
                font: counter.font
                color: counter.color
                text: ssid
            }

            Text {
                id: statusField
                visible: !rootItem.isPskEdit
                Layout.alignment: Qt.AlignRight
                width: parent.width*.1
                font: counter.font
                color: "white"
                text: disabled?"Выключена":"Включена"
            }

            Text {
                id: bssidField
                visible: !rootItem.isPskEdit
                Layout.alignment: Qt.AlignRight
                Layout.rightMargin: 10
                width: parent.width*.1
                font: counter.font
                color: "white"
                text: `bssid: ${bssid}`
            }
        }
    } // delegate
}
