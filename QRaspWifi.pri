QT += network qml quick

DEFINES += CONFIG_CTRL_IFACE
DEFINES += CONFIG_CTRL_IFACE_UNIX

INCLUDEPATH += $$PWD
DEPENDPATH += $$PWD

INCLUDEPATH	+= $$PWD/wpa_supplicant-2.6/src \
                   $$PWD/wpa_supplicant-2.6/src/utils \
                   $$PWD/wpa_supplicant-2.6/src/common

SOURCES += \
    $$PWD/qraspwifi.cpp \
    $$PWD/qwpanetworklist.cpp \
    $$PWD/qwpascanresultlist.cpp \
    $$PWD/wpa_supplicant-2.6/src/common/wpa_ctrl.c \
    $$PWD/wpa_supplicant-2.6/src/utils/os_unix.c \
    $$PWD/qwpactrliface.cpp

HEADERS += \
    $$PWD/QRaspWifi.h \
    $$PWD/qwpactrlinterface.h \
    $$PWD/qwpanetworklist.h \
    $$PWD/qwpascanresultlist.h

RESOURCES += \
    $$PWD/wifi.qrc
