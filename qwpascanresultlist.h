#ifndef QWPASCANRESULTLIST_H
#define QWPASCANRESULTLIST_H

#include <QAbstractListModel>
#include <QJsonArray>
#include <QJsonObject>

class QWpaScanResultList: public QAbstractListModel
{
    Q_OBJECT
public:
    enum WpaScanResultRoles {
        N_BSSID = Qt::UserRole+1,
        N_SSID,
        N_SIGNAL,
        N_FLAGS
    };
    Q_ENUM(WpaScanResultRoles)

public:
    QWpaScanResultList(QObject *parent = nullptr);
    virtual ~QWpaScanResultList();

    virtual QHash<int, QByteArray> roleNames() const;
    virtual int rowCount(const QModelIndex &parent = QModelIndex()) const;
    virtual QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;

    Q_INVOKABLE void slot_createNetwork(const QModelIndex &index);

    void updateNetwork(const QByteArray &buf);

private:
    QList<QJsonObject> _scanResultList;

};

#endif // QWPASCANRESULTLIST_H
