#ifndef QWPACTRLIFACE_H
#define QWPACTRLIFACE_H

#include <QDebug>

#include <QObject>
#include <QJsonObject>
#include <QTimer>
#include <QSocketNotifier>

#define CTRL_IFACE_DIR "/var/run/wpa_supplicant"

class QWpaCtrlIface : public QObject
{
    Q_OBJECT

    enum class CmdStatus: int {
        NO_ERROR = 0,
        TIMEOUT  = -2
    };

public:
    explicit QWpaCtrlIface(QObject *parent = nullptr);
    virtual ~QWpaCtrlIface();

    bool isWpaConnected() const;

    bool connectToWpaService(const char *ifname = nullptr);
    int ctrlRequest(const char *cmd, char *buf, size_t *buflen);
    int ctrlRequest(const char *cmd, QByteArray &buf);

    QString getCurrentIface() const;
    bool selectIface(const QString &iface);

    int getInterfaces(QByteArray &res);
    int getStatus(QByteArray &res);
    int getStatusVerbose(QByteArray &res);
    int logon();
    int logooff();

    // Netowrks
    int getListConfiguredNetworks(QByteArray &res);
    int selectNetwork(const QString &networkId);
    int createNetwork(QByteArray &res);
    int setNetworkVariable(const QString &netId, const QString &varName, const QString &value, bool quote = false);
    int setEnableNet(const QString &netId, bool enabled = true);
    int startPreauth(const QString &bssid);

    int removeNetwork(const QString &id);

    // Scan
    int startScan();
    int getScanResult(QByteArray &res);
    int getBSS(QByteArray &res, const QString &param = QString());
    int setAPscan(int f = 0);

    // Config
    int saveConfig();

signals:
    void wpaMessageReceive(QByteArray msg);
    void wpaConnectionStatusChanged(bool isConnected);

private slots:
    void receiveMsgs();
    void wpaPing();

private:
    int openWpaCtrlConnection(const char *ifname = nullptr);
    void setWpaConnectionStatus(bool isConnected);

private:
    bool connectedToService{false}; // флаг подключения к сервису

    QStringList _adapterList; // список адаптеров
    bool wpsAvaliable{false}; // доступно быстрое подключение по WPS

    // структуры, необходымые для подключения к wpa_supplicant - сервису
    struct wpa_ctrl *monitor_conn{nullptr};
    struct wpa_ctrl *ctrl_conn{nullptr};
    char *ctrl_iface_dir{nullptr};
    char *ctrl_iface{nullptr};

    QSocketNotifier *msgNotifier;
    QTimer *pingTimer;
};

#endif // QWPACTRLIFACE_H
