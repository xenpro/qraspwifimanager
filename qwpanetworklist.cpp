#include "qwpanetworklist.h"
#include <QDebug>

QWpaNetworkList::QWpaNetworkList(QObject *parent): QAbstractListModel(parent)
{

}

QWpaNetworkList::~QWpaNetworkList()
{

}

QHash<int, QByteArray> QWpaNetworkList::roleNames() const
{
    QHash<int, QByteArray> result;
    result.insert(N_SSID, "ssid");
    result.insert(N_BSSID, "bssid");
    result.insert(N_ID, "network_id");
    result.insert(N_SIGNAL, "signalLevel");
    result.insert(N_FLAGS, "flags");
    result.insert(N_SELECTED, "selected");
    result.insert(N_DISABLED, "disabled");
    return result;
}

int QWpaNetworkList::rowCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return 0;
    return  networkList.count();
}

QVariant QWpaNetworkList::data(const QModelIndex &index, int role) const
{
    if (!index.isValid()) return QVariant();
    if (index.row() > networkList.count()) return QVariant();
    if (!roleNames().keys().contains(role)) return QVariant();
    QString roleName = roleNames()[role];
    return networkList[index.row()][roleName];
}

void QWpaNetworkList::updateNetworks(const QByteArray &res)
{
    // clean current network list
    beginResetModel();
    networkList.clear();
    endResetModel();

    // Parse new network listvk
    QList<QByteArray> bufLines = res.split('\n');
    QList<QByteArray>::Iterator it = bufLines.begin();
    QStringList headers;
    // Parse headers
    if (it->contains('/')) { // headers exists
        QList<QByteArray> headerLines = it->split('/');
        for (auto header: headerLines)
            headers << header.simplified().replace(' ', '_');
        ++it;
    }

    for (; it != bufLines.end(); ++it) {
        QJsonObject curNet;
        QList<QByteArray> params = it->split('\t');
//        if (params.count() < headers.count())
//            continue;
        for (int i = 0; i < headers.count(); ++i)
        {

            curNet.insert(headers[i], i < params.count() ? QString(params[i]) : "");
        }

        curNet["selected"] = curNet["flags"].toString().contains("CURRENT", Qt::CaseInsensitive);
        curNet["disabled"] = curNet["flags"].toString().contains("DISABLED", Qt::CaseInsensitive);

        // insert network to list and update view
        beginInsertRows(QModelIndex(), networkList.count(), networkList.count());
        networkList.append(curNet);
        endInsertRows();
    }
}

void QWpaNetworkList::selectNetwork(int index)
{
    beginResetModel();
    QJsonObject selectedNet = networkList[index];
    QString netId = selectedNet[roleNames()[N_ID]].toString();
    emit signal_WpaSelectNetwork(netId);
    endResetModel();
}

void QWpaNetworkList::removeNetwork(int index)
{
    beginRemoveRows(QModelIndex(), index, index);
    QJsonObject removedNet = networkList[index];
    networkList.removeAt(index);
    QString netId = removedNet[roleNames()[N_ID]].toString();
    emit signal_WpaRemoveNetwork(netId);
    endRemoveRows();
}

void QWpaNetworkList::setDisabledNetwork(int index, bool disabled)
{
    beginResetModel();
    QJsonObject disabledNet = networkList[index];
    QString netId = disabledNet[roleNames()[N_ID]].toString();
    emit signal_WpaDisableNetwork(netId, disabled);
    endResetModel();
}

void QWpaNetworkList::setWpaParam(int index, QString param, QString val, bool quotes)
{
    if (index < 0 || index > networkList.count()) return;
    QJsonObject net = networkList[index];
    QString netId = net[roleNames()[N_ID]].toString();
    if (netId.isEmpty()) return;
    emit signal_setWpaParam(netId, param, val, quotes);
}
