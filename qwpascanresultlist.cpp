#include "qwpascanresultlist.h"
#include <QDebug>

QWpaScanResultList::QWpaScanResultList(QObject *parent):QAbstractListModel(parent) {}
QWpaScanResultList::~QWpaScanResultList() { }

QHash<int, QByteArray> QWpaScanResultList::roleNames() const
{
    QHash<int, QByteArray> result;
    result.insert(N_BSSID, "bssid");
    result.insert(N_SSID, "ssid");
    result.insert(N_SIGNAL, "signal_level");
    result.insert(N_FLAGS, "flags");
    return result;
}

int QWpaScanResultList::rowCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return 0;
    return _scanResultList.count();
}

QVariant QWpaScanResultList::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    if (index.row() > _scanResultList.count())
        return QVariant();

    QJsonObject net = _scanResultList[index.row()];
    switch (role) {
    case N_BSSID:
    case N_SSID:
    case N_FLAGS:
    case N_SIGNAL:
        return net[roleNames()[role]].toString();
    }
    return QVariant();
}

void QWpaScanResultList::slot_createNetwork(const QModelIndex &index)
{
    if (!index.isValid()) return;
    if (index.row() > _scanResultList.count()) return;
    // emit createNetwork(ssid, [bssid]) -> QRaspWiFi::createNetowrk(ssid, [bssid])
}

void QWpaScanResultList::updateNetwork(const QByteArray &buf)
{
    QList<QByteArray> bufLines = buf.split('\n');
    QList<QByteArray>::Iterator it = bufLines.begin();
    QStringList headers;
    // Parse headers
    if (it->contains('/')) { // headers exists
        QList<QByteArray> headerLines = it->split('/');
        for (auto header: headerLines)
            headers << header.simplified().replace(' ', '_');
        ++it;
    }

    // Cleanup old scan result
    beginResetModel();
    _scanResultList.clear();
    endResetModel();

    for (; it != bufLines.end(); ++it) {
        QJsonObject curNet;
        QList<QByteArray> params = it->split('\t');
        if (params.count() != headers.count()) // количество значений должно быть равно количеству заголовков
            continue; // если это не так, то пропускаем. Fu wpa supplicant )) // WAS: Q_ASSERT(params.count() == headers.count())
        for (int i=0;i<headers.count();++i)
            curNet.insert(headers[i], QString(params[i]));
        beginInsertRows(QModelIndex(), _scanResultList.count(), _scanResultList.count());
        _scanResultList.append(curNet);
        endInsertRows();
    }
}
