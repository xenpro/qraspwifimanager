#include "qwpactrlinterface.h"

#include <cstdio>
#include <unistd.h>

#include "common/wpa_ctrl.h"
#include "dirent.h"

QWpaCtrlIface::QWpaCtrlIface(QObject *parent) :
    QObject(parent)
{
    connectedToService=false;
    ctrl_iface_dir = strdup(CTRL_IFACE_DIR);

    // Init wpa-ping-timer
    pingTimer = new QTimer(this);
    pingTimer->setInterval(1000);
    pingTimer->setSingleShot(false);
    connect(pingTimer, &QTimer::timeout, this, &QWpaCtrlIface::wpaPing);
    // Try connect to wpa_supplicant service
    if ( ! connectToWpaService(ctrl_iface)) {
        qWarning() << "QWpaCtrlIface:: Failed to open control connection to 'wpa_supplicant' service.";
    }
    QByteArray buf;
    if (ctrlRequest("SCAN_INTERVAL 30", buf) == 0)
        qInfo() << "WiFi:scan_interval=30: " << buf;
    setAPscan(1);
}

QWpaCtrlIface::~QWpaCtrlIface()
{
    if (monitor_conn) {
        wpa_ctrl_detach(monitor_conn);
        wpa_ctrl_close(monitor_conn);
        monitor_conn = nullptr;
    }
    if (ctrl_conn) {
        wpa_ctrl_close(ctrl_conn);
        ctrl_conn = nullptr;
    }

    free(ctrl_iface);
    ctrl_iface = nullptr;

    free(ctrl_iface_dir);
    ctrl_iface_dir = nullptr;
}

bool QWpaCtrlIface::isWpaConnected() const
{
    return connectedToService;
}

bool QWpaCtrlIface::connectToWpaService(const char *ifname)
{
    setWpaConnectionStatus(openWpaCtrlConnection(ifname) == 0);
    if (connectedToService)
        pingTimer->start();
    return connectedToService;
}

int QWpaCtrlIface::openWpaCtrlConnection(const char *ifname)
{
    char *cfile;
    size_t flen;
    char buf[2048], *pos, *pos2;
    size_t len;

    if (ifname) {
        if (ifname != ctrl_iface) {
            free(ctrl_iface);
            ctrl_iface = strdup(ifname);
        }
    } else {
#ifdef CONFIG_CTRL_IFACE_UDP
        free(ctrl_iface);
        ctrl_iface = strdup("udp");
#endif /* CONFIG_CTRL_IFACE_UDP */
#ifdef CONFIG_CTRL_IFACE_UNIX
        struct dirent *dent;
        DIR *dir = opendir(ctrl_iface_dir);
        free(ctrl_iface);
        ctrl_iface = nullptr;
        if (dir) {
            while ((dent = readdir(dir))) {
#ifdef _DIRENT_HAVE_D_TYPE
                /* Skip the file if it is not a socket.
                 * Also accept DT_UNKNOWN (0) in case
                 * the C library or underlying file
                 * system does not support d_type. */
                if (dent->d_type != DT_SOCK &&
                    dent->d_type != DT_UNKNOWN)
                    continue;
#endif /* _DIRENT_HAVE_D_TYPE */

                if (strcmp(dent->d_name, ".") == 0 ||
                    strcmp(dent->d_name, "..") == 0)
                    continue;
                ctrl_iface = strdup(dent->d_name);
                break;
            }
            closedir(dir);
        }
#endif /* CONFIG_CTRL_IFACE_UNIX */
#ifdef CONFIG_CTRL_IFACE_NAMED_PIPE
        struct wpa_ctrl *ctrl;
        int ret;

        free(ctrl_iface);
        ctrl_iface = NULL;

        ctrl = wpa_ctrl_open(NULL);
        if (ctrl) {
            len = sizeof(buf) - 1;
            ret = wpa_ctrl_request(ctrl, "INTERFACES", 10, buf,
                                   &len, NULL);
            if (ret >= 0) {
                connectedToService = true;
                buf[len] = '\0';
                pos = strchr(buf, '\n');
                if (pos)
                    *pos = '\0';
                ctrl_iface = strdup(buf);
            }
            wpa_ctrl_close(ctrl);
        }
#endif /* CONFIG_CTRL_IFACE_NAMED_PIPE */
    }

    if (ctrl_iface == nullptr) {
#ifdef CONFIG_NATIVE_WINDOWS
        static bool first = true;
        if (first && !serviceRunning()) {
            first = false;
            if (QMessageBox::warning(
                    this, qAppName(),
                    tr("wpa_supplicant service is not "
                       "running.\n"
                       "Do you want to start it?"),
                    QMessageBox::Yes | QMessageBox::No) ==
                QMessageBox::Yes)
                startService();
        }
#endif /* CONFIG_NATIVE_WINDOWS */
        return -1;
    }

#ifdef CONFIG_CTRL_IFACE_UNIX
    flen = strlen(ctrl_iface_dir) + strlen(ctrl_iface) + 2;
    cfile = static_cast<char *>(malloc(flen));
    if (cfile == nullptr)
        return -1;
    snprintf(cfile, flen, "%s/%s", ctrl_iface_dir, ctrl_iface);
#else /* CONFIG_CTRL_IFACE_UNIX */
    flen = strlen(ctrl_iface) + 1;
    cfile = (char *) malloc(flen);
    if (cfile == NULL)
        return -1;
    snprintf(cfile, flen, "%s", ctrl_iface);
#endif /* CONFIG_CTRL_IFACE_UNIX */

    if (ctrl_conn) {
        wpa_ctrl_close(ctrl_conn);
        ctrl_conn = nullptr;
    }

    if (monitor_conn) {
        delete msgNotifier;
        msgNotifier = nullptr;
        wpa_ctrl_detach(monitor_conn);
        wpa_ctrl_close(monitor_conn);
        monitor_conn = nullptr;
    }

    ctrl_conn = wpa_ctrl_open(cfile);
    if (ctrl_conn == nullptr) {
        free(cfile);
        return -1;
    }
    monitor_conn = wpa_ctrl_open(cfile);
    free(cfile);
    if (monitor_conn == nullptr) {
        wpa_ctrl_close(ctrl_conn);
        return -1;
    }
    if (wpa_ctrl_attach(monitor_conn)) {
        qWarning() << "Failed to attach to wpa_supplicant";
        wpa_ctrl_close(monitor_conn);
        monitor_conn = nullptr;
        wpa_ctrl_close(ctrl_conn);
        ctrl_conn = nullptr;
        return -1;
    }

#if defined(CONFIG_CTRL_IFACE_UNIX) || defined(CONFIG_CTRL_IFACE_UDP)
    msgNotifier = new QSocketNotifier(wpa_ctrl_get_fd(monitor_conn), QSocketNotifier::Read, this);
    connect(msgNotifier, SIGNAL(activated(int)), SLOT(receiveMsgs()));
#endif

    _adapterList.clear();
    _adapterList.append(ctrl_iface);

    len = sizeof(buf) - 1;
    if (wpa_ctrl_request(ctrl_conn, "INTERFACES", 10, buf, &len, nullptr) >= 0) {
        buf[len] = '\0';
        pos = buf;
        while (*pos) {
            pos2 = strchr(pos, '\n');
            if (pos2)
                *pos2 = '\0';
            if (strcmp(pos, ctrl_iface) != 0)
                _adapterList.append(pos);
            if (pos2)
                pos = pos2 + 1;
            else
                break;
        }
    }

    len = sizeof(buf) - 1;
    if (wpa_ctrl_request(ctrl_conn, "GET_CAPABILITY eap", 18, buf, &len, nullptr) >= 0) {
        buf[len] = '\0';
        QString res(buf);
        QStringList types = res.split(QChar(' '));
        bool wps = types.contains("WSC");
        wpsAvaliable = wps; // доступно быстрое подключение по WPS
    }

    return 0;
}

void QWpaCtrlIface::setWpaConnectionStatus(bool isConnected)
{
    if (connectedToService == isConnected)
        return;

    connectedToService = isConnected;

    if (connectedToService)
        pingTimer->start();
    else
        pingTimer->stop();

    emit wpaConnectionStatusChanged(connectedToService);
}

int QWpaCtrlIface::ctrlRequest(const char *cmd, char *buf, size_t *buflen) {
    int ret;
    if (ctrl_conn == nullptr)
        return -3;
    ret = wpa_ctrl_request(ctrl_conn, cmd, strlen(cmd), buf, buflen, nullptr);
    if (ret == -2)
        qWarning() << "ctrlRequest::" << cmd << " :command timed out.";
    else if (ret < 0)
        qWarning() << "ctrlRequest::" << cmd << " :command failed.";
    return ret;
}

int QWpaCtrlIface::ctrlRequest(const char *cmd, QByteArray &buf)
{
    if (!isWpaConnected()) return -10;
    buf.resize(4096*2);
    size_t buflen = static_cast<size_t>(buf.size());
    int ret = ctrlRequest(cmd, buf.data(), &buflen);
    buf.truncate(static_cast<int>(buflen));
    buf = buf.trimmed();
    buf.squeeze();
    return ret;
}

QString QWpaCtrlIface::getCurrentIface() const
{
    if (ctrl_iface)
        return QString(ctrl_iface);
    else
        return QString();
}

bool QWpaCtrlIface::selectIface(const QString &iface)
{
    return openWpaCtrlConnection(qPrintable(iface)) == 0;
}

int QWpaCtrlIface::getInterfaces(QByteArray &res)
{
    int ret = ctrlRequest("INTERFACES", res);
    return ret;
}

int QWpaCtrlIface::getStatus(QByteArray &res)
{
    if ( ! connectedToService )
        return -1;
    res.resize(265); res.fill('\n');
    int ret = ctrlRequest("STATUS", res);
    return ret;
}

int QWpaCtrlIface::getStatusVerbose(QByteArray &res)
{
    if ( ! connectedToService ) return -1;
    QByteArray buf(265, '\n');
    int ret = ctrlRequest("STATUS-VERBOSE", buf);
    if (ret == 0)
        res = buf;
    return ret;
}

int QWpaCtrlIface::logon()
{
    QByteArray buf(265, '\n');
    return ctrlRequest("LOGON", buf);
}

int QWpaCtrlIface::logooff()
{
    QByteArray buf(265, '\n');
    return ctrlRequest("LOGOFF", buf);
}

int QWpaCtrlIface::getListConfiguredNetworks(QByteArray &res)
{
    return ctrlRequest("LIST_NETWORKS", res);
}

int QWpaCtrlIface::selectNetwork(const QString &networkId)
{
    QByteArray buf;
    QString cmd(networkId);
    if (!cmd.contains(QRegExp("^\\d+$")))
        cmd = "any";
    cmd.prepend("SELECT_NETWORK ");
    return ctrlRequest(qPrintable(cmd), buf);
}

int QWpaCtrlIface::createNetwork(QByteArray &res)
{
    return ctrlRequest("ADD_NETWORK", res);
}

int QWpaCtrlIface::setNetworkVariable(const QString &netId, const QString &varName, const QString &value, bool quote)
{
    QByteArray res;
    QString cmd = QString("SET_NETWORK %1 %2 %3%4%5").arg(netId).arg(varName).arg(quote?"\"":"").arg(value).arg(quote?"\"":"");
    int ret = ctrlRequest(qPrintable(cmd), res);
    return ret;
}

int QWpaCtrlIface::setEnableNet(const QString &netId, bool enabled)
{
    QByteArray cmd, res;
    cmd = enabled?"ENABLE_NETWORK ":"DISABLE_NETWORK ";
    cmd.append(qPrintable(netId));
    return ctrlRequest(cmd, res);
}

int QWpaCtrlIface::startPreauth(const QString &bssid)
{
    QByteArray res;
    QByteArray cmd("PREAUTH ");
    cmd.append(qPrintable(bssid));
    int ret = ctrlRequest(cmd, res);
    return ret;
}

int QWpaCtrlIface::removeNetwork(const QString &id)
{
    QByteArray res;
    QByteArray cmd("REMOVE_NETWORK ");
    cmd.append(qPrintable(id));
    int ret = ctrlRequest(cmd, res);
    qInfo() << cmd << res;
    return  ret;
}

int QWpaCtrlIface::startScan()
{
    QByteArray buf(128, '\n');
    return ctrlRequest("SCAN", buf);
}

int QWpaCtrlIface::getScanResult(QByteArray &res)
{
    return ctrlRequest("SCAN_RESULTS", res);
}

int QWpaCtrlIface::getBSS(QByteArray &res, const QString &param)
{
    QByteArray cmd("BSS ");
    if (!param.isEmpty())
        cmd.append(qPrintable(param));
    return ctrlRequest(cmd, res);
}

int QWpaCtrlIface::setAPscan(int f)
{
    int ret;
    QByteArray buf(128, '\n');
    switch (f) {
    case 0:
        ret = ctrlRequest("AP_SCAN 0", buf);
        break;
    case 1:
        ret = ctrlRequest("AP_SCAN 1", buf);
        break;
    case 2:
        ret = ctrlRequest("AP_SCAN 2", buf);
        break;
    default:
        ret = ctrlRequest("AP_SCAN 0", buf);
        break;
    }
    return ret;
}

int QWpaCtrlIface::saveConfig()
{
    QByteArray res;
    return ctrlRequest("SAVE_CONFIG", res);
}

void QWpaCtrlIface::receiveMsgs()
{
    char buf[1024];
    size_t len;
    while (monitor_conn && wpa_ctrl_pending(monitor_conn) > 0) {
        len = sizeof(buf) - 1;
        if (wpa_ctrl_recv(monitor_conn, buf, &len) == 0) {
            buf[len] = '\0';
            QByteArray _msg(buf);
            emit wpaMessageReceive(_msg.trimmed().simplified());
        }
    }
}

void QWpaCtrlIface::wpaPing()
{
    char buf[10];
    size_t len;
    len = sizeof(buf) - 1;
    if (ctrlRequest("PING", buf, &len) < 0)  {
        qWarning() << "wpa-PING failed - trying to reconnect";
        if (openWpaCtrlConnection(ctrl_iface) >= 0) {
            qInfo() << "Reconnected successfully";
            setWpaConnectionStatus(true);
        }
        else
            setWpaConnectionStatus(false);
        return;
    }
}
