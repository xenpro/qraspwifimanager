#include "QRaspWifi.h"
#include <QDeadlineTimer>
#include <QQmlContext>

enum {
    AUTH_NONE_OPEN,
    AUTH_NONE_WEP,
    AUTH_NONE_WEP_SHARED,
    AUTH_IEEE8021X,
    AUTH_WPA_PSK,
    AUTH_WPA_EAP,
    AUTH_WPA2_PSK,
    AUTH_WPA2_EAP
};

QRaspWifi::QRaspWifi(QQmlApplicationEngine *parent, const QString &rootItemObjectName):
    QObject(parent),
    wpaCtrl(new QWpaCtrlIface(this)),
    _scanResultListModel(new QWpaScanResultList(this)),
    _networkListModel(new QWpaNetworkList(this))
{
    // Create timers
    auto statusTimer = new QTimer(this);
    rssiMeterTimer = new QTimer(this);

    // main connections
    connect(wpaCtrl, &QWpaCtrlIface::wpaMessageReceive, this, &QRaspWifi::wpaMessageHandler);
    parent->rootContext()->setContextProperty("WiFiCtrl",this);
    parent->rootContext()->setContextProperty("WiFiScanResultModel", _scanResultListModel);
    parent->rootContext()->setContextProperty("WiFiNetworkModel", _networkListModel);
    // Model requests connections
    connect(_networkListModel, &QWpaNetworkList::signal_WpaRemoveNetwork, this, &QRaspWifi::slot_wpaRemoveNetwork, Qt::DirectConnection);
    connect(_networkListModel, &QWpaNetworkList::signal_WpaSelectNetwork, this, &QRaspWifi::slot_selectNetwork, Qt::DirectConnection);
    connect(_networkListModel, &QWpaNetworkList::signal_setWpaParam, this, &QRaspWifi::slot_setWpaParam, Qt::DirectConnection);
    connect(_networkListModel, &QWpaNetworkList::signal_WpaDisableNetwork, this, &QRaspWifi::slot_setDisabledNetwork, Qt::DirectConnection);

    // load default qml from res
    setCustomHelpComponent();
    rootQuickObjectName = rootItemObjectName;

    // Update current values and states
    slot_startScan();
    slot_updateNetworkList();
    slot_updateStatus();

    // start timers
    statusTimer->setInterval(5000);
    connect(statusTimer, &QTimer::timeout, this, &QRaspWifi::slot_updateStatus);
    statusTimer->start();

    connect(rssiMeterTimer, &QTimer::timeout, this, &QRaspWifi::slot_updateWpaRssiState);
    rssiMeterTimer->setInterval(2000);
    rssiMeterTimer->start();

    if (getInterfaces().contains("wlan0")) { // auto connect to wlan0 interface
        selectInterface("wlan0");
    }
}

void QRaspWifi::setCustomHelpComponent(const QUrl &qmlUrl)
{
     if (qmlUrl.isValid())
         wifiComponentUrl = qmlUrl;
}

QJsonObject QRaspWifi::getStatus() const
{
    return status;
}

QString QRaspWifi::getWpaStatus()
{
    return status["wpa_state"].toString();
}

QString QRaspWifi::getCurrentIface() const
{
    return wpaCtrl->getCurrentIface();
}

QStringList QRaspWifi::getInterfaces() const
{
    QByteArray buf;
    wpaCtrl->getInterfaces(buf);
    QStringList result;
    for (auto item : buf.split('\n'))
        result.append(QString::fromLocal8Bit(item));
    return result;
}

bool QRaspWifi::selectInterface(const QString &iface)
{
    return wpaCtrl->selectIface(iface);
}

bool QRaspWifi::createNewWiFiConnection(const QString &ssid, const QString &psk, const QString bbsid)
{
    QByteArray buf;
    wpaCtrl->createNetwork(buf);
}

QRaspWifi::RssiState QRaspWifi::getRssi() const
{
    return rssi;
}

void QRaspWifi::updateRssiState(QRaspWifi::RssiState state)
{
    if (rssi == state)
        return;
    rssi = state;
    emit rssiChanged();
}

void QRaspWifi::slot_updateStatus()
{
    QByteArray res;
    wpaCtrl->getStatus(res);
    QList<QByteArray> statusParams = res.split('\n');
    QJsonObject statusResult;
    for (auto param: statusParams) {
        QList<QByteArray> paramAndValue = param.split('=');
        statusResult.insert(paramAndValue.first(), QString(paramAndValue[1]));
    }

    if (status["wpa_state"].toString().contains("DISCONNECTED", Qt::CaseInsensitive)) {
        updateRssiState(Offline);
    }
    else
        if (status["wpa_state"].toString().contains("INACTIVE", Qt::CaseInsensitive)) {
        updateRssiState(Offline);
    }
    else
        if (status["wpa_state"].toString().contains("SCANNING", Qt::CaseInsensitive)) {
        updateRssiState(Scaning);
    }
    else
        if (status["wpa_state"].toString().contains("ASSOCIATING", Qt::CaseInsensitive)) {
        updateRssiState(Acquiring);
    }
    else
        if (status["wpa_state"].toString().contains("COMPLETED", Qt::CaseInsensitive)) {
        if (rssi < Connected)
            updateRssiState(Connected);
    }

    if (status != statusResult) {
        status = statusResult;
        emit wpaStatusChanged();
    }

}

void QRaspWifi::slot_updateNetworkList()
{
    QByteArray res;
    int ret = wpaCtrl->getListConfiguredNetworks(res);
    if (ret != 0) {
        qWarning() << "ERR: Can't get list of configured networkd!";
        return;
    }
    _networkListModel->updateNetworks(res);

    emit wpaStatusChanged();
    slot_updateStatus();
}

void QRaspWifi::slot_createNetwork(const QString &ssid, const QString &flags, const QString bssid)
{
    QByteArray res;
    int ret = wpaCtrl->createNetwork(res);
    if (ret != 0) {
        qWarning() << "ERR: Can't create network!";
        return;
    }

    QString newNetId(res.simplified());

    ret = wpaCtrl->selectNetwork(newNetId);
    if (ret != 0) {
        qWarning() << "ERR: Can't select new network!";
        return;
    }


    ret = wpaCtrl->setNetworkVariable(newNetId, "ssid", ssid, true);
    if (ret != 0) {
        qWarning() << "ERR: Can't set ssid for new network!";
        return;
    }

    if (!bssid.isEmpty()) {
        ret = wpaCtrl->setNetworkVariable(newNetId, "bssid", bssid, false);
        if (ret != 0) {
            qWarning() << "ERR: Can't set bssid for new network!";
            return;
        }
    }

    int auth, encr = 0;
    if (flags.indexOf("[WPA2-EAP") >= 0)
        auth = AUTH_WPA2_EAP;
    else if (flags.indexOf("[WPA-EAP") >= 0)
        auth = AUTH_WPA_EAP;
    else if (flags.indexOf("[WPA2-PSK") >= 0)
        auth = AUTH_WPA2_PSK;
    else if (flags.indexOf("[WPA-PSK") >= 0)
        auth = AUTH_WPA_PSK;
    else
        auth = AUTH_NONE_OPEN;

    if (flags.indexOf("-CCMP") >= 0)
        encr = 1;
    else if (flags.indexOf("-TKIP") >= 0)
        encr = 0;
    else if (flags.indexOf("WEP") >= 0) {
        encr = 1;
        if (auth == AUTH_NONE_OPEN)
            auth = AUTH_NONE_WEP;
    } else
        encr = 0;

    // set params for connect
    const char *key_mgmt = nullptr, *proto = nullptr, *pairwise = nullptr;
    switch (auth) {
    case AUTH_NONE_OPEN:
    case AUTH_NONE_WEP:
    case AUTH_NONE_WEP_SHARED:
        key_mgmt = "NONE";
        break;
    case AUTH_IEEE8021X:
        key_mgmt = "IEEE8021X";
        break;
    case AUTH_WPA_PSK:
        key_mgmt = "WPA-PSK";
        proto = "WPA";
        break;
    case AUTH_WPA_EAP:
        key_mgmt = "WPA-EAP";
        proto = "WPA";
        break;
    case AUTH_WPA2_PSK:
        key_mgmt = "WPA-PSK";
        proto = "WPA2";
        break;
    case AUTH_WPA2_EAP:
        key_mgmt = "WPA-EAP";
        proto = "WPA2";
        break;
    }

    // set flags to config

    if (auth == AUTH_NONE_WEP_SHARED)
        wpaCtrl->setNetworkVariable(newNetId, "auth_alg", "SHARED", false);
    else
        wpaCtrl->setNetworkVariable(newNetId, "auth_alg", "OPEN", false);

    if (auth == AUTH_WPA_PSK || auth == AUTH_WPA_EAP ||
        auth == AUTH_WPA2_PSK || auth == AUTH_WPA2_EAP) {
        if (encr == 0)
            pairwise = "TKIP";
        else
            pairwise = "CCMP";
    }

    if (proto)
        wpaCtrl->setNetworkVariable(newNetId, "proto", proto, false);
    if (key_mgmt)
        wpaCtrl->setNetworkVariable(newNetId, "key_mgmt", key_mgmt, false);
    if (pairwise) {
        wpaCtrl->setNetworkVariable(newNetId, "pairwise", pairwise, false);
        wpaCtrl->setNetworkVariable(newNetId, "group", "TKIP CCMP WEP104 WEP40", false);
    }

    ret = wpaCtrl->saveConfig();
    if (ret != 0) {
        qWarning() << "ERR: Can't save config!";
        return;
    }

    qInfo() << ":::" << "New network was saved";

    emit wpaStatusChanged();
    slot_updateStatus();
}

void QRaspWifi::slot_createNetworkWithPSK(const QString &ssid, const QString &flags, const QString &psk, const QString bssid)
{
    QByteArray res;
    int ret = wpaCtrl->createNetwork(res);
    if (ret != 0) {
        qWarning() << "ERR: Can't create network!";
        return;
    }

    QString newNetId(res.simplified());

    ret = wpaCtrl->selectNetwork(newNetId);
    if (ret != 0) {
        qWarning() << "ERR: Can't select new network!";
        return;
    }


    ret = wpaCtrl->setNetworkVariable(newNetId, "ssid", ssid, true);
    if (ret != 0) {
        qWarning() << "ERR: Can't set ssid for new network!";
        return;
    }

    if (!bssid.isEmpty()) {
        ret = wpaCtrl->setNetworkVariable(newNetId, "bssid", bssid, false);
        if (ret != 0) {
            qWarning() << "ERR: Can't set bssid for new network!";
            return;
        }
    }

    int auth, encr = 0;
    if (flags.indexOf("[WPA2-EAP") >= 0)
        auth = AUTH_WPA2_EAP;
    else if (flags.indexOf("[WPA-EAP") >= 0)
        auth = AUTH_WPA_EAP;
    else if (flags.indexOf("[WPA2-PSK") >= 0)
        auth = AUTH_WPA2_PSK;
    else if (flags.indexOf("[WPA-PSK") >= 0)
        auth = AUTH_WPA_PSK;
    else
        auth = AUTH_NONE_OPEN;

    if (flags.indexOf("-CCMP") >= 0)
        encr = 1;
    else if (flags.indexOf("-TKIP") >= 0)
        encr = 0;
    else if (flags.indexOf("WEP") >= 0) {
        encr = 1;
        if (auth == AUTH_NONE_OPEN)
            auth = AUTH_NONE_WEP;
    } else
        encr = 0;

    // set params for connect
    const char *key_mgmt = nullptr, *proto = nullptr, *pairwise = nullptr;
    switch (auth) {
    case AUTH_NONE_OPEN:
    case AUTH_NONE_WEP:
    case AUTH_NONE_WEP_SHARED:
        key_mgmt = "NONE";
        break;
    case AUTH_IEEE8021X:
        key_mgmt = "IEEE8021X";
        break;
    case AUTH_WPA_PSK:
        key_mgmt = "WPA-PSK";
        proto = "WPA";
        break;
    case AUTH_WPA_EAP:
        key_mgmt = "WPA-EAP";
        proto = "WPA";
        break;
    case AUTH_WPA2_PSK:
        key_mgmt = "WPA-PSK";
        proto = "WPA2";
        break;
    case AUTH_WPA2_EAP:
        key_mgmt = "WPA-EAP";
        proto = "WPA2";
        break;
    }

    // set flags to config

    if (auth == AUTH_NONE_WEP_SHARED)
        wpaCtrl->setNetworkVariable(newNetId, "auth_alg", "SHARED", false);
    else
        wpaCtrl->setNetworkVariable(newNetId, "auth_alg", "OPEN", false);

    if (auth == AUTH_WPA_PSK || auth == AUTH_WPA_EAP ||
        auth == AUTH_WPA2_PSK || auth == AUTH_WPA2_EAP) {
        if (encr == 0)
            pairwise = "TKIP";
        else
            pairwise = "CCMP";
    }

    if (proto)
        wpaCtrl->setNetworkVariable(newNetId, "proto", proto, false);
    if (key_mgmt)
        wpaCtrl->setNetworkVariable(newNetId, "key_mgmt", key_mgmt, false);
    if (pairwise) {
        wpaCtrl->setNetworkVariable(newNetId, "pairwise", pairwise, false);
        wpaCtrl->setNetworkVariable(newNetId, "group", "TKIP CCMP WEP104 WEP40", false);
    }

    slot_setWpaParam(newNetId, "psk", psk, true);

    ret = wpaCtrl->saveConfig();
    if (ret != 0) {
        qWarning() << "ERR: Can't save config!";
        return;
    }

    qInfo() << ":::" << "New network was saved";

    emit wpaStatusChanged();
    slot_updateStatus();
}

void QRaspWifi::slot_selectNetwork(const QString &netId)
{
    qDebug() << Q_FUNC_INFO << netId;
    if (wpaCtrl->selectNetwork(netId) != 0) {
        qWarning() << "Can't wpa_select network with id " << netId;
    };
    slot_updateNetworkList();
    slot_updateStatus();
}

void QRaspWifi::slot_setDisabledNetwork(const QString &netId, bool disabled)
{
    qDebug() << Q_FUNC_INFO << netId;
    if (wpaCtrl->setEnableNet(netId, !disabled) != 0) {
        qWarning() << "Err: WPA can't set DISABLED to " << disabled;
    };
    slot_updateNetworkList();
    slot_updateStatus();
}

void QRaspWifi::slot_setWpaParam(const QString &netId, const QString &param, const QString &value, bool q)
{
    if (wpaCtrl->setNetworkVariable(netId, param, value, q) != 0) {
        qWarning() << "Err: Wpa can't set param " << param << " with value " << value << " (use quotes:" << q << ")";
    }
    slot_updateStatus();
}

void QRaspWifi::slot_wpaGetStatus()
{
    emit wpaStatusChanged();
}

void QRaspWifi::slot_startScan()
{
    wpaCtrl->setAPscan(1);
    wpaCtrl->startScan();
    emit wpaStatusChanged();
    slot_updateStatus();
}

void QRaspWifi::slot_wpaRemoveNetwork(QString id)
{
    if (wpaCtrl->removeNetwork(id) != 0) {
        qWarning() << "Can't remove network with id " << id;
    }
    slot_updateStatus();
}

void QRaspWifi::slot_showWiFiManager()
{
    getWiFiManagerItem();
}

void QRaspWifi::slot_closeWifiManager()
{
    wifitem->deleteLater();
    wifitem = nullptr;
}

void QRaspWifi::slot_saveConfig()
{
    if ( wpaCtrl->saveConfig() != 0) {
        qWarning() << "ERR: Can't save wpa_supplicant config file!";
    }
    slot_updateStatus();
}

void QRaspWifi::slot_DISCONNECT()
{
    QByteArray res;
    wpaCtrl->ctrlRequest("DISCONNECT", res);
    slot_updateStatus();
}

void QRaspWifi::slot_REASSOCIATE()
{
    QByteArray res;
    wpaCtrl->ctrlRequest("REASSOCIATE", res);
    slot_updateStatus();
}

void QRaspWifi::slot_RECONNECT()
{
    QByteArray res;
    wpaCtrl->ctrlRequest("RECONNECT", res);
    slot_updateStatus();
}

void QRaspWifi::wpaMessageHandler(QByteArray msg)
{
    if (msg.contains("CTRL-EVENT-STATE-CHANGE"))
        emit wpaStatusChanged();
    else
        if (msg.contains("CTRL-EVENT-SCAN-RESULTS"))
        getScanResult();
//    else
//        if (msg.contains("CTRL-EVENT-SCAN-STARTED"))
//        qInfo() << "CTRL-EVENT-SCAN-STARTED";
    slot_updateStatus();
}

void QRaspWifi::slot_updateWpaRssiState()
{
    if (!wpaCtrl->isWpaConnected()) {
        updateRssiState(Offline);
        return;
    }

    // if (rssi < Connected) return; // can't check signal until not connected

    QByteArray res;
    wpaCtrl->ctrlRequest("SIGNAL_POLL", res);

    QList<QByteArray> signal_poll = res.split('\n');
    int rssi_value = 0;
    for (auto line: signal_poll) {
        if (line.startsWith("RSSI")) {
            rssi_value = atoi(line.split('=').last());
            break;
        }
    }

    if (rssi_value != 0) {
        if (rssi_value >= -60)
            updateRssiState(SignalExcellent);
        else if (rssi_value >= -68)
            updateRssiState(SignalGood);
        else if (rssi_value >= -76)
            updateRssiState(SignalOk);
        else if (rssi_value >= -84)
            updateRssiState(SignalWeak);
        else
            updateRssiState(SignalNone);
    }
}

QQuickItem *QRaspWifi::getWiFiManagerItem()
{
    if (!wpaCtrl->isWpaConnected()) { // нет соединения с wpa-supplicant
        if (wifitem)
            wifitem->deleteLater();
        wifitem=nullptr;
        return nullptr;
    }

    if (wifitem != nullptr)
        return wifitem;

    QQmlApplicationEngine *engine = qobject_cast<QQmlApplicationEngine*>(parent());
    if (engine->rootObjects().isEmpty()) {
        qWarning() << Q_FUNC_INFO << "rootObject is empty!";
        return nullptr;
    }
    QQuickItem *quickRootItem = engine->rootObjects().first()->findChild<QQuickItem*>(rootQuickObjectName);
    if (!quickRootItem) {
        qWarning() << Q_FUNC_INFO << "Can't get root object! (QQuickItem*)";
        return nullptr;
    }
    QQmlComponent *componentLoader = new QQmlComponent(engine, QUrl(wifiComponentUrl), this);

    QDeadlineTimer dltimer;
    dltimer.setDeadline(4000);
    while (componentLoader->status() != QQmlComponent::Ready) {
        if (dltimer.remainingTime())
            return nullptr;
        qApp->processEvents();
    }
    wifitem = qobject_cast<QQuickItem*>(componentLoader->create());
    wifitem->setParentItem(quickRootItem);
    if ( wifitem == nullptr)
        qWarning() << "Can't create help component!" << componentLoader->errorString();
    return wifitem;
}

void QRaspWifi::getScanResult()
{
    QByteArray res;
    wpaCtrl->getScanResult(res);
    //    int scanResultCounter=res.count('\n');
    _scanResultListModel->updateNetwork(res);

    //    for (int c=0; c<scanResultCounter-1;++c) {
    //        qDebug() << "GET BSID INFO " << c+1;
    //        wpaCtrl->getBSS(res, QString::number(c));
    //        qDebug() << "\t\tgetBSID: " << res.split('\n') << endl << "-===============================-";
    //    }
}
